
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

// Used materials:
// Parsing and Formatting a Byte Array into Binary: https://www.tutorialspoint.com/parsing-and-formatting-a-byte-array-into-binary-in-java
// Inspiration: https://www.programiz.com/dsa/huffman-coding

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

    private LinkedHashMap<Byte, Integer> charactersCount = new LinkedHashMap();
    private LinkedHashMap<Byte, String> charactersCodes = new LinkedHashMap();
    private LinkedHashMap<String, Byte> codesCharacters = new LinkedHashMap();
    private LinkedHashMap<Byte, Node> charactersObjects = new LinkedHashMap();
    private byte[] encode;
    private byte[] decoded = {};
    private int initialSize = 0;
    private int encodedSize = 0;
    private Byte[] keys;

    private Node tree;
    private int prefixcode = 2;


    /**
     * Constructor to build the Huffman code for a given bytearray.
     *
     * @param original source data
     */
    Huffman(byte[] original) {
        fillCharactersCount(original);

        sortCharactersCount();

        createTree();
        fillCharactersCodes();
    }

    private void fillCharactersCodes() {
        if (keys.length < 2) {
            charactersCodes.put(keys[0], "1");
            codesCharacters.put("1", keys[0]);
            return;
        }

        for (Byte key : keys) {
            Node node = charactersObjects.get(key);
            StringBuffer code = new StringBuffer();

            while (node.parent != null) {
                code.append(node.parentBranch);
                node = node.parent;
            }
            code = code.reverse();

            charactersCodes.put(key, code.toString());
            codesCharacters.put(code.toString(), key);

        }
    }

    /**
     * Create tree
     */
    private void createTree() {
        keys = charactersCount.keySet().toArray(new Byte[0]);

        List<Node> nodes = new ArrayList<>();


        for (Byte key : keys) {
            Node node = new Node(key, charactersCount.get(key));
            charactersObjects.put(key, node);
            nodes.add(node);
        }

        if (nodes.size() < 2) {
            tree = nodes.get(0);
            return;
        }

        tree = mergeNodes(nodes);
    }


    /**
     * Merge list of Nodes to the tree
     *
     * @param nodes list of Node to merge
     * @return root Node of tree
     */
    private Node mergeNodes(List<Node> nodes) {
        while (nodes.size() > 1) {
            nodes = nodes.stream()
                    .sorted(Comparator.comparing(a -> a.info))
                    .collect(Collectors.toList());


            Node one = nodes.get(0);
            Node zero = nodes.get(1);
            nodes.remove(zero);
            nodes.remove(one);


            Node newNode = mergeTwoNodes(one, zero);
            nodes.add(newNode);

        }

        return nodes.get(0);

    }

    /**
     * Merge list of Nodes to the tree
     *
     * @param one  Node to set as child on 1 branch
     * @param zero Node to set as child on 0 branch
     * @return Node parent of merged nodes
     */
    private Node mergeTwoNodes(Node one, Node zero) {
        Node parent = new Node(one.info + zero.info, one, zero);
        zero.parent = parent;
        zero.parentBranch = 0;
        one.parent = parent;
        one.parentBranch = 1;
        return parent;

    }


    /**
     * Sort charactersCount Map
     */
    private void fillCharactersCount(byte[] original) {
        for (byte b : original) {
            charactersCount.put(b, charactersCount.getOrDefault(b, 0) + 1);
        }
        initialSize = original.length * 8;
    }

    /**
     * Sort charactersCount Map
     */
    private void sortCharactersCount() {
        LinkedHashMap<Byte, Integer> sorted = new LinkedHashMap<>();

        charactersCount.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .forEachOrdered(x -> sorted.put(x.getKey(), x.getValue()));
        charactersCount = sorted;
    }

    /**
     * Length of encoded data in bits.
     *
     * @return number of bits
     */
    public int bitLength() {
        return encodedSize;
    }


    /**
     * Encoding the byte array using this prefixcode.
     *
     * @param origData original data
     * @return encoded data
     */
    public byte[] encode(byte[] origData) {
        StringBuffer code = new StringBuffer();

        for (byte b : origData) {
            code.append(charactersCodes.get(b));
        }
        encodedSize = code.length();

        encode = new BigInteger(code.toString(), prefixcode).toByteArray();

        return encode;
    }

    /**
     * Decoding the byte array using this prefixcode.
     *
     * @param encodedData encoded data
     * @return decoded data (hopefully identical to original)
     */
    public byte[] decode(byte[] encodedData) {
        BigInteger binary = new BigInteger(encodedData);
        String binaryStr = binary.toString(prefixcode);

        char[] charArray = binaryStr.toCharArray();
        LinkedList<Byte> res = new LinkedList<>();

        StringBuffer code = new StringBuffer();

        for (char c : charArray) {
            code.append(c);
            if (codesCharacters.containsKey(code.toString())) {
                res.add(codesCharacters.get(code.toString()));
                code = new StringBuffer();
            }

        }

        byte[] array = new byte[res.size()];
        for (int i = 0; i < res.size(); i++) {
            array[i] = res.get(i);
        }
        return array;
    }

    private class Node {
        private byte code;
        private int info;
        private Node one;
        private Node zero;
        private Node parent;
        private int parentBranch;


        Node(int i, Node one, Node zero) {
            info = i;
            this.one = one;
            this.zero = zero;
        }

        Node(byte c, int i) {
            code = c;
            info = i;
        }


    }

    /**
     * Main method.
     */
    public static void main(String[] params) {
        String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
        byte[] orig = tekst.getBytes();
        Huffman huf = new Huffman(orig);
        byte[] kood = huf.encode(orig);
        byte[] orig2 = huf.decode(kood);
        // must be equal: orig, orig2
        System.out.println(Arrays.equals(orig, orig2));
        int lngth = huf.bitLength();
        System.out.println("Length of encoded data in bits: " + lngth);
        // TODO!!! Your tests here!
    }
}

